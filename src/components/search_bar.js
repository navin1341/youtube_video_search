import React,{Component} from 'react';

class SearchBar extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={ term:'' };
    }
    render()
    {
        return (
        <div className="search-bar">
        <input 
        onChange={event => this.onInputChange(event.target.value)}/>
     <button >Search</button>
        </div>
        );
    }
    onInputChange(term)
    {
      this.setState({term});
      this.props.onSearchTermChange(term);  {/* callback function calling to props in index.js */}
    }
   }

export default SearchBar;