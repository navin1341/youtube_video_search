import _ from 'lodash';
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar'
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list'
import VideoDetail from './components/video_detail'
const Api_key='AIzaSyBQLo5eHXt2AMhTIkkKIjYLLL6pjUDDlMo';

class App extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            video:[],
            selectedVideo:null
        }
        this.videoSearch('phir mulaqat');

    }

    videoSearch(term)
    {
        YTSearch({key:Api_key,term:term},(video) =>{
            this.setState({
                video:video,
                selectedVideo:video[0]
            });
        })
    }

    render(){
        const videoSearch= _.debounce((term) => {this.videoSearch(term)},400);
return (
   <div>
    <SearchBar onSearchTermChange= {videoSearch}/>  {/* callback function it is taking term from searchbar and redirect to videosearch function*/}
    <VideoDetail video={this.state.selectedVideo} />
    <VideoList
    onVideoSelect={selectedVideo => this.setState({selectedVideo})}
    video={this.state.video} />
    </div>
);
}
}
ReactDOM.render(<App />,document.querySelector('.container'));
